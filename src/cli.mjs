import { readFileSync } from 'node:fs'
import Nedb from 'nedb-promise'

const db = new Nedb({
  filename: process.env.DATABASE || 'data',
  autoload: true,
})

const state = []

let nextThreadId = 1
let nextPostId = 0

; {
  const threads = await db.find({ is: 'thread' })
  threads.sort((a, b) => a.id - b.id)
  if (threads.length > 0) nextThreadId = threads[threads.length - 1].id + 1
  for (const { id, content, deleted } of threads) if (!deleted) state[id] = { id, content, posts: [] }
  const posts = await db.find({ is: 'post' })
  posts.sort((a, b) => a.id - b.id)
  if (posts.length > 0) nextPostId = posts[posts.length - 1].id + 1
  for (const post of posts) {
    const { id, threadId, content, inReplyTo, deleted } = post
    if (!deleted) state[threadId]?.posts.push({ id, content, inReplyTo })
  }
}

const cmd = {
  async export (id) {
    id = Number(id)
    const thread = await db.findOne({ is: 'thread', id })
    const posts = await db.find({ is: 'post', threadId: id })
    delete thread._id
    for (const post of posts) delete post._id
    console.log(JSON.stringify({ thread, posts }))
  },
  async import () {
    const { thread, posts } = JSON.parse(readFileSync(0).toString())
    const id = nextThreadId++
    await db.insert({ ...thread, id, imported: true })
    for (const post of posts) {
      await db.insert({ ...post, id: nextPostId++, threadId: id, imported: true })
    }
  },
  async importId (id) {
    id = Number(id)
    const thread1 = await db.findOne({ is: 'thread', id })
    if (thread1) throw new Error(thread.content)
    const { thread, posts } = JSON.parse(readFileSync(0).toString())
    await db.insert({ ...thread, id, imported: true })
    for (const post of posts) {
      await db.insert({ ...post, id: nextPostId++, threadId: id, imported: true })
    }
  },
  async delete (id) {
    id = Number(id)
    await db.remove({ is: 'thread', id })
    await db.remove({ is: 'post', threadId: id }, { multi: true })
  },
  async shift (id) {
    id = Number(id)
    const thread = await db.findOne({ is: 'thread', id })
    if (thread) throw new Error(thread.content)
    await db.update({ is: 'thread', id: { $gt: id } }, { $inc: { id: -1 } }, { multi: true })
    await db.update({ is: 'post', threadId: { $gt: id } }, { $inc: { threadId: -1 } }, { multi: true })
  },
  async unshift (id) {
    id = Number(id)
    await db.update({ is: 'thread', id: { $gt: id } }, { $inc: { id: 1 } }, { multi: true })
    await db.update({ is: 'post', threadId: { $gt: id } }, { $inc: { threadId: 1 } }, { multi: true })
  },
  async close (id) {
    id = Number(id)
    await db.update({ is: 'thread', id }, { $set: { closed: true } })
  },
}

const [ _node, _cli, name, ...args ] = process.argv

await cmd[name](...args)
